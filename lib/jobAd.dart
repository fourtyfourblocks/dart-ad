library jobAd;

import "package:log4dart/log4dart_vm.dart";
import 'package:mustache/mustache.dart' as mustache;
import 'package:start/start.dart' as server;
import "dart:io";

class JobAd {
    static final _logger = LoggerFactory.getLoggerFor(JobAd);

    int counter = 0;

    void start(String host, int port, [List<String> args = const []]) {
        _logger.info("lets start");
        args.forEach((arg) => _logger.infoFormat("apps args : %s", [arg]));

        var source = '{{#names}}<div>{{lastname}}, {{firstname}}</div>{{/names}}';
        var template = mustache.parse(source);
        var model = {
            'names': [
                {
                    'firstname': 'Greg', 'lastname': 'Lowe'
                },
                {
                    'firstname': 'Bob', 'lastname': 'Johnson'
                }
            ]
        };

        _logger.info("starting server");
        server
        .start(host: 'localhost', port: 8080)
        .then((server.Server app) {
            app.static('web');
            app.get('/hello/:name/:lastname?')
            .listen((request) {
                model['names'].add({
                                       'firstname': request.param('name'),
                                       'lastname': request.param('lastname')
                                   });
                var output = template.renderString(model);

                request.response
                .header('Content-Type', 'text/html; charset=UTF-8')
                .send(output);
            });
        });
    }
}
